FROM openjdk:11.0-slim
RUN mkdir /app
WORKDIR /app
COPY target/helmesDemo-0.0.1.jar /app/

ENV JAVA_OPTS=''
ENV TZ="Europe/Tallinn"
EXPOSE 8080
ENTRYPOINT exec java $JAVA_OPTS -jar /app/helmesDemo-0.0.1.jar