package ee.vallsalu.helmesDemo.controller;

import ee.vallsalu.helmesDemo.model.UserSubmission;
import ee.vallsalu.helmesDemo.repository.SectorsRepo;
import ee.vallsalu.helmesDemo.repository.UserSubmissionRepo;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@AllArgsConstructor
@Controller
@RequestMapping({"/", "/index"})
public class FormController {

  SectorsRepo sectorsRepo;
  UserSubmissionRepo submissionRepo;

  @GetMapping
  public String showForm(Model model, HttpSession session) {
    if (Objects.isNull(session.getAttribute("allSectors"))) {
      session.setAttribute("allSectors", sectorsRepo.findAll());
    }
    if (Objects.isNull(model.getAttribute("message"))) {
      model.addAttribute("message", false);
      model.addAttribute("success", true);
    }
    Optional<UserSubmission> submission = Optional.empty();
    if (Objects.nonNull(session.getAttribute("submissionId"))) {
      submission = submissionRepo.findById((String) session.getAttribute("submissionId"));
      if (submission.isEmpty()) {
        model.addAttribute("message", true);
        model.addAttribute("success", false);
      }
    }
    model.addAttribute("userSubmission", submission.orElseGet(UserSubmission::new));
    return "index";
  }

  @PostMapping
  public String submitForm(@ModelAttribute UserSubmission submission,
      RedirectAttributes redirectAttributes, HttpSession session) {
    if (!StringUtils.hasText(submission.getId())) {
      submission.setId(null);
    }
    try {
      submissionRepo.save(submission);
      session.setAttribute("submissionId", submission.getId());
      redirectAttributes.addFlashAttribute("success", true);
    } catch (Exception e) {
      e.printStackTrace();
      redirectAttributes.addFlashAttribute("success", false);
    }
    redirectAttributes.addFlashAttribute("message", true);
    return "redirect:/index";
  }

}
