package ee.vallsalu.helmesDemo.model;

import java.util.Collection;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Submissions")
public class UserSubmission {

  @Id
  private String id;
  private String name;
  private Collection<Long> selectedSectors;
  private Boolean agreeToTerms;

}
