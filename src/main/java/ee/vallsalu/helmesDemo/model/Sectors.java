package ee.vallsalu.helmesDemo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.util.HtmlUtils;

@Data
@Document(collection = "Sectors")
public class Sectors {

  @Id
  private String id;
  private String name;
  private Long value;

  public Sectors(String name, Long value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return HtmlUtils.htmlUnescape(name);
  }

}
