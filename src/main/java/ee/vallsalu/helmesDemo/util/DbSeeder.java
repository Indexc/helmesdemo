package ee.vallsalu.helmesDemo.util;

import ee.vallsalu.helmesDemo.model.Sectors;
import ee.vallsalu.helmesDemo.repository.SectorsRepo;
import java.util.Arrays;
import java.util.Collection;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
@Profile("DbSeeder")
public class DbSeeder implements CommandLineRunner {

  private SectorsRepo sectorsRepo;

  @Override
  public void run(String... args) throws Exception {
    System.out.println("reSeeding DB Sectors table/collection!");

    Collection<Sectors> sectors = Arrays.asList(
        new Sectors("Manufacturing", 1L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Construction materials", 19L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Electronics and Optics", 18L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Food and Beverage", 6L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bakery &amp; confectionery products",
            342L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beverages", 43L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fish &amp; fish products",
            42L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Meat &amp; meat products",
            40L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Milk &amp; dairy products",
            39L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other", 437L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sweets &amp; snack food",
            378L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Furniture", 13L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bathroom/sauna", 389L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bedroom", 385L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Children's room", 390L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kitchen", 98L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Living room", 101L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Office", 392L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other (Furniture)", 394L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Outdoor", 341L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project furniture", 99L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Machinery", 12L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Machinery components", 94L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Machinery equipment/tools",
            91L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manufacture of machinery",
            224L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maritime", 97L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aluminium and steel workboats",
            271L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Boat/Yacht building",
            269L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ship repair and conversion",
            230L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal structures", 93L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other", 508L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Repair and maintenance service", 227L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Metalworking", 11L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Construction of metal structures",
            67L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Houses and buildings", 263L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal products", 267L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Metal works", 542L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CNC-machining",
            75L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Forgings, Fasteners",
            62L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gas, Plasma, Laser cutting",
            69L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MIG, TIG, Aluminum welding",
            66L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Plastic and Rubber", 9L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Packaging", 54L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic goods", 556L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic processing technology",
            559L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blowing", 55L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Moulding",
            57L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastics welding and processing",
            53L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plastic profiles", 560L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Printing", 5L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Advertising", 148L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Book/Periodicals printing",
            150L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Labelling and packaging printing",
            145L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Textile and Clothing", 7L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clothing", 44L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Textile", 45L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Wood", 8L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other (Wood)", 337L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wooden building materials",
            51L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wooden houses", 47L),
        new Sectors("Other", 3L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Creative industries", 37L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Energy technology", 29L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Environment", 33L),
        new Sectors("Service", 2L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Business services", 25L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Engineering", 35L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Information Technology and Telecommunications", 28L),
        new Sectors(
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data processing, Web portals, E-marketing",
            581L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Programming, Consultancy",
            576L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Software, Hardware", 121L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telecommunications", 122L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Tourism", 22L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Translation services", 141L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;Transport and Logistics", 21L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Air", 111L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rail", 114L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Road", 112L),
        new Sectors("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Water", 113L)
    );

    //drop all sectors
    this.sectorsRepo.deleteAll();

    //add sectors to DB
    this.sectorsRepo.saveAll(sectors);
  }

}
