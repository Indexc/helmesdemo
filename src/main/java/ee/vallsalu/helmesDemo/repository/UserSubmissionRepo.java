package ee.vallsalu.helmesDemo.repository;

import ee.vallsalu.helmesDemo.model.UserSubmission;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSubmissionRepo extends MongoRepository<UserSubmission, String> {

  Optional<UserSubmission> findById(String id);

}
