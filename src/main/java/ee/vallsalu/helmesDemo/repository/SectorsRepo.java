package ee.vallsalu.helmesDemo.repository;

import ee.vallsalu.helmesDemo.model.Sectors;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectorsRepo extends MongoRepository<Sectors, String> {

}
