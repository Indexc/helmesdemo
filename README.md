#helmesDemo

Tasks:
1. Correct all of the deficiencies in index.html

2. "Sectors" selectbox: <br>
2.1. Add all the entries from the "Sectors" selectbox to database <br>
2.2. Compose the "Sectors" selectbox using data from database

3. Perform the following activities after the "Save" button has been pressed: <br>
3.1. Validate all input data (all fields are mandatory) <br>
3.2. Store all input data to database (Name, Sectors, Agree to terms) <br>
3.3. Refill the form using stored data <br>
3.4. Allow the user to edit his/her own data during the session


After completing the tasks, please provide us with:
1. Full database dump (structure and data)
2. Source code

##Database
database:  MongoDB <br><br>
Dump located `db` in directory, restoring database from dump
```
mongorestore --uri="mongodb://{some-other-server}/helmesDemo" ./db/helmesDemo
```

##Docker
Added `Dockerfile` and `docker-compose.yml` for easy deployment. `docker-compose` will deploy both app and database. <br><br>
Running docker-compose:
```
docker-compose up
```

Database restore from dump must be executed separately. 
Container "mongo" contains volume `db-dump` for easy access database dump in the container. 
```
docker exec -i mongo sh -c 'mongorestore -d helmesDemo /data/dump/helmesDemo'
```
